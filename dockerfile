FROM php:7.4-fpm

# Cài đặt composer để quản lý dependencies
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Thiết lập thư mục làm việc
WORKDIR /var/www/html

# Sao chép mã nguồn ứng dụng PHP vào image
COPY . /var/www/html


